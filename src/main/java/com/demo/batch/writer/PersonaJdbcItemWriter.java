package com.demo.batch.writer;

import com.demo.batch.domain.Persona;
import com.demo.batch.domain.Tarjeta;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import javax.sql.DataSource;
import org.springframework.batch.item.ItemWriter;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class PersonaJdbcItemWriter implements ItemWriter<Persona> {

    private NamedParameterJdbcOperations namedParameterJdbcTemplate;
    private final Map<String, String> mapTarjeta = new ConcurrentHashMap<>();

    public PersonaJdbcItemWriter(DataSource dataSource) {
        if (namedParameterJdbcTemplate == null) {
            this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        }
    }

    @Override
    public void write(List<? extends Persona> personas) throws Exception {
        SqlParameterSource[] personaParameters = SqlParameterSourceUtils.createBatch(personas);
        namedParameterJdbcTemplate.batchUpdate(
                "INSERT INTO PERSONA VALUES (:id, :nombre, :edad)", personaParameters);

        personas.stream().forEach(persona -> {
            List<Tarjeta> tarjetas = persona.getTarjetas().stream().filter(tarjeta -> {
                return mapTarjeta.putIfAbsent(tarjeta.getCodigo(), tarjeta.getNombre()) == null;
            }).collect(Collectors.toList());

            SqlParameterSource[] tarjetaParameters = SqlParameterSourceUtils.createBatch(tarjetas);
            namedParameterJdbcTemplate.batchUpdate(
                    "INSERT INTO TARJETA VALUES (:codigo, :nombre)", tarjetaParameters);

            SqlParameterSource[] personaTarjetaParameters = SqlParameterSourceUtils.createBatch(persona.getPersonaTrajeta());
            namedParameterJdbcTemplate.batchUpdate(
                    "INSERT INTO PERSONA_TARJETA VALUES (:id, :codigo)", personaTarjetaParameters);
        });
    }
}
