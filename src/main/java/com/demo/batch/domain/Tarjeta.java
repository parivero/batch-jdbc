package com.demo.batch.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Tarjeta {
    
    private String codigo;
    private String nombre;

}
