package com.demo.batch.domain;

import java.util.List;
import lombok.Data;

@Data
public class Persona {
    
    private Long id;
    private String nombre;
    private int edad;
    private String tarjetasPlanas;
    private List<Tarjeta> tarjetas;
    private List<PersonaTarjeta> personaTrajeta;

}
