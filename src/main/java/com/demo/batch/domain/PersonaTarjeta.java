package com.demo.batch.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PersonaTarjeta {

    private Long id;
    private String codigo;

}
