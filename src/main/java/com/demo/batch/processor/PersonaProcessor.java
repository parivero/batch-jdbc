package com.demo.batch.processor;

import com.demo.batch.domain.Persona;
import com.demo.batch.domain.Tarjeta;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersonaProcessor implements ItemProcessor<Persona, Persona> {

    @Autowired
    private AtomicLong secuencia;

    @Override
    public Persona process(Persona persona) throws Exception {
        persona.setId(secuencia.incrementAndGet());
        String[] splitTarjetas = persona.getTarjetasPlanas().split(";");
        List<Tarjeta> tarjetas = Arrays.asList(splitTarjetas).stream().map(tarjeta -> {
            String[] tarj = tarjeta.split(",");
            return new Tarjeta(tarj[0], tarj[1]);
        }).collect(Collectors.toList());
        persona.setTarjetas(tarjetas);
        return persona;
    }

}
