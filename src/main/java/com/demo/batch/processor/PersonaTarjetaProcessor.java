package com.demo.batch.processor;

import com.demo.batch.domain.Persona;
import com.demo.batch.domain.PersonaTarjeta;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class PersonaTarjetaProcessor implements ItemProcessor<Persona, Persona> {

    @Override
    public Persona process(Persona persona) throws Exception {
        List<PersonaTarjeta> collect = persona.getTarjetas().stream().map(tarjeta -> {
            PersonaTarjeta personaTarjeta = new PersonaTarjeta(persona.getId(), tarjeta.getCodigo());
            return personaTarjeta;
        }).collect(Collectors.toList());
        persona.setPersonaTrajeta(collect);
        return persona;
    }

}
