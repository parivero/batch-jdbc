package com.demo.batch;

import com.demo.batch.processor.PersonaProcessor;
import com.demo.batch.domain.Persona;
import com.demo.batch.processor.PersonaTarjetaProcessor;
import com.demo.batch.writer.PersonaJdbcItemWriter;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;
import javax.sql.DataSource;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableBatchProcessing
public class JobConfig {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private PersonaProcessor personaProcessor;
    @Autowired
    private PersonaTarjetaProcessor personaTarjetaProcessor;
    @Value("${com.demo.batch.chunk}")
    private int chunck;
    @Value("${com.demo.batch.threads}")
    private int threads;
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public AtomicLong secuencia() {
        return new AtomicLong();
    }

    @Bean
    public ItemReader<Persona> personaReader() {
        Resource r = new ClassPathResource("persona-data.csv");
        return new FlatFileItemReaderBuilder<Persona>()
                .name("reader")
                .resource(r)
                .targetType(Persona.class)
                .delimited().delimiter("|").names(new String[]{"nombre", "edad", "tarjetasPlanas"})
                .saveState(false)
                .build();
    }

    @Bean
    public CompositeItemProcessor<Persona, Persona> procesor() {
        CompositeItemProcessor<Persona, Persona> compositeItemProcessor = new CompositeItemProcessor<>();
        compositeItemProcessor.setDelegates(Arrays.asList(personaProcessor, personaTarjetaProcessor));
        return compositeItemProcessor;
    }

    @Bean
    public ItemWriter personaWriter() {
        return new PersonaJdbcItemWriter(dataSource);
    }

    @Bean
    public Step stepPersona(StepBuilderFactory stepBuilders) {
        
        return this.stepBuilderFactory.get("step")
                .allowStartIfComplete(true)
                .<Persona, Persona>chunk(chunck)
                .reader(personaReader())
                .processor(procesor())
                .writer(personaWriter())
                .taskExecutor(executor())
                .build();
    }

    @Bean
    public Job personaJob(JobBuilderFactory jobBuilders, Step stepPersona) {
        return this.jobBuilderFactory.get("personaJob").start(stepPersona).build();
    }
    
    @Bean
    public TaskExecutor executor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(threads);
        executor.setMaxPoolSize(threads);
        executor.afterPropertiesSet();
        return executor;
    }

}
