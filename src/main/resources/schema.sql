create table persona (
    id BIGINT PRIMARY KEY,
    nombre VARCHAR(500),
    edad INT
);

create table tarjeta (
    codigo VARCHAR(10) PRIMARY KEY,
    nombre VARCHAR(500)
);

create table persona_tarjeta (
    id BIGINT,
    codigo VARCHAR(10),
    UNIQUE (id, codigo),
    FOREIGN KEY (codigo) REFERENCES tarjeta(codigo)
);

